import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckequipmentFormComponent } from './checkequipment-form.component';

describe('CheckequipmentFormComponent', () => {
  let component: CheckequipmentFormComponent;
  let fixture: ComponentFixture<CheckequipmentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckequipmentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckequipmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
