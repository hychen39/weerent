import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckequipmentComponent } from './checkequipment.component';

describe('CheckequipmentComponent', () => {
  let component: CheckequipmentComponent;
  let fixture: ComponentFixture<CheckequipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckequipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckequipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
