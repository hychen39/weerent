import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkequipment',
  templateUrl: './checkequipment.component.html',
  styleUrls: ['./checkequipment.component.css']
})
export class CheckequipmentComponent implements OnInit {

  constructor(private _router: Router) {
    
  }
   call(){
   this._router.navigate(['info/checkequipment/form']);
 }

 ngOnInit(): void {
  
 }
}
