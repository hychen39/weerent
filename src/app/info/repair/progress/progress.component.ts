import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {
  date!:string;
  article!:string;
  progress!:string;
  complete!:string;

  constructor() { }

  ngOnInit(): void {
    this.date ="2021/3/6";
    this.article="燈泡";
    this.progress="已修復";
    this.complete="2021/3/7";
  }

}
