import { Component, OnInit } from '@angular/core';
import { Routes } from '@angular/router';



@Component({
  selector: 'app-feehistory',
  templateUrl: './feehistory.component.html',
  styleUrls: ['./feehistory.component.css']
})
export class FeehistoryComponent implements OnInit {
  rent: number;
  fee: number;
  total: number;
  constructor() { }

  ngOnInit(): void {
    this.rent = 9000;
    this.fee = 700;
    this.total = this.rent+this.fee;
  }

}
