import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeehistoryComponent } from './feehistory.component';

describe('FeehistoryComponent', () => {
  let component: FeehistoryComponent;
  let fixture: ComponentFixture<FeehistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeehistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeehistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
