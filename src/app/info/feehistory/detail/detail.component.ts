import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  date: string;
  type: string;
  money: number;
  interval:string;
  elefee: number;
  usedele:number;
  constructor() { }

  ngOnInit(): void {
    this.date = '2000/12/1';
    this.type = '電費';
    this.elefee = 5;
    this.usedele = 100;
    this.money = this.elefee * this.usedele;
    this.interval = '12/1~12/31';
  }

}
