import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RepairComponent } from './info/repair/repair.component';
import { FeehistoryComponent } from './info/feehistory/feehistory.component';
import { CheckequipmentComponent } from './info/checkequipment/checkequipment.component';
import { MainComponent } from './info/main/main.component';
import { RentComponent } from './info/feehistory/rent/rent.component';
import { FeeComponent } from './info/feehistory/fee/fee.component';
import { TotalComponent } from './info/feehistory/total/total.component';
import { DetailComponent } from './info/feehistory/detail/detail.component';
import { ProgressComponent } from './info/repair/progress/progress.component';
import { RepairdetailComponent } from './info/repair/repairdetail/repairdetail.component';
import { HomeComponent } from './home/home.component';
import { CheckequipmentFormComponent } from './info/checkequipment/checkequipment-form/checkequipment-form.component';


@NgModule({
  declarations: [
    AppComponent,
    RepairComponent,
    FeehistoryComponent,
    CheckequipmentComponent,
    MainComponent,
    RentComponent,
    FeeComponent,
    TotalComponent,
    DetailComponent,
    ProgressComponent,
    RepairdetailComponent,
    HomeComponent,
    CheckequipmentFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
