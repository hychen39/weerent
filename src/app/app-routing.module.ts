import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckequipmentComponent } from './info/checkequipment/checkequipment.component';
import { FeehistoryComponent } from './info/feehistory/feehistory.component';
import { RepairComponent } from './info/repair/repair.component';
import { MainComponent } from './info/main/main.component';
import { RentComponent } from './info/feehistory/rent/rent.component';
import { FeeComponent } from './info/feehistory/fee/fee.component';
import { TotalComponent } from './info/feehistory/total/total.component';
import { DetailComponent } from './info/feehistory/detail/detail.component';
import { ProgressComponent } from './info/repair/progress/progress.component';
import { RepairdetailComponent } from './info/repair/repairdetail/repairdetail.component';
import { HomeComponent } from './home/home.component';
import { CheckequipmentFormComponent } from './info/checkequipment/checkequipment-form/checkequipment-form.component';


const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'info/main', component: MainComponent},
  {path: 'info/checkequipment', component: CheckequipmentComponent,
    children: [ {path: 'form', component: CheckequipmentFormComponent}]},
  {path: 'info/feehistory', component: FeehistoryComponent,
    children: [ {path: 'rent', component: RentComponent},
                {path: 'fee', component: FeeComponent},
                {path: 'total', component: TotalComponent},
                {path: 'detail', component: DetailComponent}
  ]},
  {path: 'info/repair', component: RepairComponent,
    children: [ {path: 'progress', component: ProgressComponent},
                {path: 'repairdetail', component: RepairdetailComponent}
  ]},
  {path: '', redirectTo: 'home', pathMatch:'full'}//outlet
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
